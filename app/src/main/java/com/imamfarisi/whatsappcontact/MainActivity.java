package com.imamfarisi.whatsappcontact;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Contact> contactList = new ArrayList<>();
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        recyclerView = findViewById(R.id.recyclerview);

        setData();
    }

    private void setData() {
        Contact dataSpiderman = new Contact("Spiderman", "Homecoming", "MOBILE", R.drawable.spiderman);
        Contact dataThor = new Contact("Thor", "Ragnarok", "MOBILE", R.drawable.thor);
        Contact dataIronMan = new Contact("Ironman", "Bukan Besi Biasa", "MOBILE", R.drawable.ironman);
        Contact dataCaptain = new Contact("Captain America", "Civil War", "MOBILE", R.drawable.captain);
        Contact dataFlash = new Contact("The Flash", "Speed is Everything", "MOBILE", R.drawable.flash);

        contactList.add(dataSpiderman);
        contactList.add(dataThor);
        contactList.add(dataIronMan);
        contactList.add(dataCaptain);
        contactList.add(dataFlash);

        RecyclerviewAdapter mAdapter = new RecyclerviewAdapter(contactList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
    }
}
